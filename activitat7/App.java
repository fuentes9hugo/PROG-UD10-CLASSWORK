package com.prog.activitat7;

import com.prog.activitat1.MySqlConnection;
import com.prog.activitat5.PasswordHelper;
import com.prog.activitat3.Validator;

import java.sql.*;
import java.util.Scanner;

public class App {


    public static void main( String[] args ) {

        MySqlConnection mySqlConnection = new MySqlConnection("crm_db", "user_java", "password_java");
        Connection connection = mySqlConnection.getConnection();


        printAllUsers(connection);

        String userEmail = scanEmail();
        String userPassword = scanPassword();


        if(validatePassword(connection,userEmail,userPassword)){

            System.out.println("OK");
        } else {

            System.out.println("BAD LOGIN");
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void printAllUsers(Connection connection){

        String query = "SELECT id, email, firstName, lastName FROM User ORDER BY id";

        try {

            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_UPDATABLE);

            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                System.out.println(rs.getInt("id") + "\t-- " + rs.getString("email") + "\t-- " + rs.getString("firstName") + "\t-- " + rs.getString("lastName"));
            }

        } catch (SQLException e){

            e.printStackTrace();

        }

    }

    private static String getHash(Connection connection, String email ){

        String query = "SELECT password FROM User WHERE email = ?";

        try {

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, email);

            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {

                return rs.getString("password");

            }

        } catch (SQLException e){

            e.printStackTrace();

        }

        return null;

    }

    private static String scanEmail(){

        Validator validator = new Validator();

        Scanner scannerInput = new Scanner(System.in);

        String response;

        System.out.print("Email: ");

        validator.validateString(response = scannerInput.next(),0,45);

        return response;

    }

    private static String scanPassword(){

        Validator validator = new Validator();

        Scanner scannerInput = new Scanner(System.in);

        String response;

        System.out.print("Password: ");

        validator.validateString(response = scannerInput.next(),0,160);

        return response;

    }

    private static boolean validatePassword(Connection connection, String userEmail, String userPassword){

        return PasswordHelper.verifySHA(userPassword,getHash(connection,userEmail));

    }
}
